# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
[MyGirl](https://app.mygirl.tech/) website is more suitable for wide screens.
## Preview
![Preview](https://gitlab.com/breatfr/mygirl/-/raw/main/docs/preview-v1.xx.jpg)

Changelog
- https://gitlab.com/breatfr/MyGirl/-/blob/main/CHANGELOG.md

Help / comment / ask etc...
- https://discord.com/channels/1172088835358863410/1172482585918709840

## Customisations available
# chat page
- background choice
	- hearts background
	- no background
	- custom background
- background color choice (can be use with background choice too)
- bubbles font color of your choice
- bubbles font size of your choice
- blur content (bubbles texts and images) to share in privacy
- AI's bubbles background color of your choice
- our bubbles background color of your choice
- textarea font size of your choice
# settings page
- layout modification to avoid scroll

## How to use in few steps
1. Install Stylus browser extension
    - Chromium based browsers link: https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - many more
    - Firefox based browsers link: https://addons.mozilla.org/firefox/addon/styl-us/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - many more

2. Go on [UserStyles.world](https://userstyles.world/style/14551) website and click on `Install` under the preview picture or open the [GitLab version](https://gitlab.com/breatfr/mygirl/-/raw/main/css/mygirl-wide-screen-v1.xx.user.css).

3. To update the theme, open the `Stylus Management` window and click on `Check for update` and follow the instructions or just wait 24h to automatic update

4. Enjoy :)
# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>